# Просмотр модели

Чтобы сгенерировать urdf модель по шаблону, нужно запустить
```
xacro --inorder model.xacro > model.urdf
```

Для просмотра модели в RViz используется команда
```
roslaunch urdf_tutorial display.launch model:='model.urdf'
```

# Внешний вид робота

![Picture](pic.png)
